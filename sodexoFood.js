const request = require("request");

const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;
const day = date.getDate();

let url = "https://www.sodexo.fi/ruokalistat/output/daily_json/12812/"+year+"/"+month+"/"+day+"/fi";

request(url, function(error, response, body) {
    if (error) console.log(error);
    let sodexoFood = {};
    const sodexo = JSON.parse(body);
    //console.log(sodexo)
    const menu = sodexo["courses"];

    for (let i = 0; i < menu.length; i++) {
        if (menu[i]["category"] === "Popular" || menu[i]["category"] === "Inspiring") { // Takes only 2,60 foods
            let foods = [];
            foods.push(menu[i]["title_fi"]);

            if (menu[i]["desc_fi"].endsWith("\r\n")) {
                foods.push(menu[i]["desc_fi"].slice(0, -2));
            } else {
                foods.push(menu[i]["desc_fi"]);
            }

            foods.push(menu[i]["properties"]);
            sodexoFood["Linjasto" + i] = foods;
        }
    }
    if (Object.keys(sodexoFood).length === 0) { // If there's no food, probably weekend
        sodexoFood["Linjasto0"] = ["-"];
        sodexoFood["Linjasto1"] = ["-"];
    }
    // exports the food array
    //console.log(sodexoFood);
    exports.sodexoFood = sodexoFood;
});