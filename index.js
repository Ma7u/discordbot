const config = require("./config.json");
const Discord = require("discord.js");
const ytdl = require("ytdl-core");
let amica = require("./amicaFood");
let sodexo = require("./sodexoFood");
let newton = require("./newtonFood");

/*
Refreshes food menus every 5 hours
 */
async function delFoodRequires () {
    delete require.cache[require.resolve("./amicaFood")];
    delete require.cache[require.resolve("./sodexoFood")];
    delete require.cache[require.resolve("./newtonFood")];
}
async function getFoodRequires () {
    amica = require("./amicaFood");
    sodexo = require("./sodexoFood");
    newton = require("./newtonFood");
}
setInterval(function () {
    delFoodRequires().then(getFoodRequires());
}, 60 * 60 * 1000 * 5);


const client = new Discord.Client();

const prefix = "!";

const streamOptions = {passes: 5, volume: 0.2};

let dispatcher;
let musicQueue = [];
let punishList = {};

function playSong(connection, message) {
    // Tries to play the link. If link is invalid, deletes it from queue.
    try {
        dispatcher = connection.playStream(ytdl(musicQueue[0], { filter : 'audioonly' }), streamOptions);
        dispatcher.on("end", () => {
            musicQueue.shift();
            if (musicQueue[0]) playSong(connection, message);
            else connection.disconnect();
        });
    } catch (err) {
        console.log(err);
        musicQueue.shift();
    }
}



client.on('ready', async () => {
    console.log('I am ready!');
});

client.on('message', async message => {
    // Return if message if from bot or there's no prefix
    if (message.author.bot) return;
    if (message.content[0] !== prefix) return;

    let wholeMessage = message.content.split(" ");
    let command = wholeMessage[0].toLowerCase();
    let args = wholeMessage.slice(1);
    //console.log(args);

    if (command === `${prefix}raidiviipale`) {
        message.channel.send({
          file: "./pics/jaadytetty_raidi_viipale.png"  
        });
        return
    }
    
    if (command === `${prefix}avatar`) {
        message.reply(message.author.avatarURL);
        return
    }

    if (command === `${prefix}batman`) {
        message.channel.send({
            file: "./pics/batman.png"
        });
        return
    }

    if (command === `${prefix}batoque`) {
        message.channel.send({
            file: "./pics/batoque.png"
        });
        return
    }

    if (command === `${prefix}tonnikala`) {
        message.channel.send({
            file: "./pics/tonnikala.png"
        });
        return
    }

    if (command === `${prefix}smile`) {
        message.channel.send({
            file: "./pics/hymy.png"
        });
        return
    }

    if (command === `${prefix}limuniittitehdas`) {
        message.channel.send({
            file: "./pics/limuniitti.png"
        });
        return
    }

    if (command === `${prefix}puuropora`) {
        message.channel.send({
            file: "./pics/puuropora.png"
        });
        return
    }

    if (command === `${prefix}deleteall`) {
        if (!message.member.hasPermission("ADMINSTRATOR")) return message.channel.send("You don't have permission.");
        // Take the name from text channel that is going to be deleted
        let channelName = message.channel.name;
        // Deletes the channel and makes a new one with the same name
        message.channel.delete("Deleted messages.");
        message.guild.createChannel(channelName, "text");
        return
    }

    if (command === `${prefix}stfu`) {
        message.delete();
        message.channel.send("Stfu gay " + args[0]);
        return
    }

    if (command === `${prefix}food`) {
        let amicaFood = amica.amicaFood;
        let newtonFood = newton.newtonFood;
        let sodexoFood = sodexo.sodexoFood;

        try {
            let amicaEmbed = new Discord.RichEmbed()
            .setAuthor("Mitäs tänään syötäisiin: " + amicaFood["date"])
            .addField("Reaktori päivä:",
                amicaFood["Linjasto0"].join("\n") + "\n\n" +
                amicaFood["Linjasto1"].join("\n"))
            .addField("Reaktori ilta: ",
                amicaFood["Iltaruoka8"].join("\n") + "\n\n" +
                amicaFood["Iltaruoka9"].join("\n"));
            message.channel.send({embed: amicaEmbed});
        } catch (err) {
            // Probably empty list in json
            console.log(err);
        }

        try {
            let sodexoEmbed = new Discord.RichEmbed()
            .addField("Sodexo:",
                sodexoFood["Linjasto0"].join("\n") + "\n\n" +
                sodexoFood["Linjasto1"].join("\n"));
            message.channel.send({embed: sodexoEmbed});
        } catch (err) {
            console.log(err);
        }
        
        try {
            let newtonEmbed = new Discord.RichEmbed()
            .addField("Newton:",
                newtonFood["Linjasto0"].join("\n") + "\n\n" +
                newtonFood["Linjasto1"].join("\n"));
            message.channel.send({embed: newtonEmbed});
        } catch (err) {
            console.log(err);
        }
    }

    if (command === `${prefix}seppo`) {
        if (!message.member.voiceChannel) return message.channel.send("Join a voice channel first.");
        musicQueue.push("https://www.youtube.com/watch?v=taQvvuQD8DM%22");
        if (musicQueue.length <= 1) message.member.voiceChannel.join().then(connection => {
            playSong(connection, message);
        return
    });
    }

    if (command === `${prefix}mute`) {
        const memberToMute = message.mentions.members.first();
        try {
            if (memberToMute.mute === true) memberToMute.setMute(false).then(message.channel.send("User unmuted."));
            else memberToMute.setMute(true).then(message.channel.send("User muted."));
        } catch (e) {
            if (e instanceof ReferenceError) {
                // User not in voice channel
            } else {
                // Something else
                console.log(e)
            }
            
        }
        return
    }

    if (command === `${prefix}jäähy`) {
        let timeForPunish = args[1]; // How long the punish lasts(seconds)
        if (timeForPunish >= 60) timeForPunish = 60;
        const time = new Date().getTime() + (1000 * timeForPunish); // Time when the punish ends
        const memberToPunish = message.mentions.members.first();
        const memberID = memberToPunish.id
        try {
            if (memberToPunish in punishList) return message.channel.send("User is already being punished.");
            if (memberToPunish.voiceChannel === undefined) return message.channel.send("User is not in a voice channel");
            memberToPunish.setVoiceChannel("291644567860543499").then(message.channel.send(`${memberToPunish} is being punished for ${timeForPunish} seconds.`));
            punishList[memberID] = time;
        } catch (e) {
                console.log(e);
            }
    }

    if (command === `${prefix}name`) {
        message.delete();
        const memberToName = message.mentions.members.first();
        let nick = args[1];
        if (nick === undefined) nick = "";
        try {
            memberToName.setNickname(nick);
        } catch (e) {
            console.log(e);
        }
    }

    if (command === `${prefix}play`) {
        if (args.length !== 1 && musicQueue.length === 0) return message.channel.send("Provide only a link.");
        if (!message.member.voiceChannel) return message.channel.send("Join a voice channel first.");

        // Adds link to queue.
        musicQueue.push(args[0]);
        message.channel.send("Song added to queue.");
        if (musicQueue.length <= 1) message.member.voiceChannel.join().then(connection => {
            playSong(connection, message);

        });
        message.delete();
        return
    }

    if (command === `${prefix}skip`) {
        dispatcher.end();
        return
    }

    if (command === `${prefix}pause`) {
        dispatcher.pause();
        return
    }

    if (command === `${prefix}resume`) {
        dispatcher.resume();
        return
    }

    if (command === `${prefix}stop`) {
        if (message.guild.voiceConnection) message.guild.voiceConnection.disconnect();
        return
    }

    if (command === `${prefix}queue`) {
        musicQueue.length === 1 ? message.channel.send("There's " + musicQueue.length + " song in queue.") :
            message.channel.send("There's " + musicQueue.length + " songs in queue.");
        return
    }

    if (command === `${prefix}help`) {
        let embed = new Discord.RichEmbed()
            .setAuthor("The best bot ever")
            .addField("Bot commands:",
                "!batman\n!batoque\n!tonnikala\n!puuropora\n!avatar\n" +
                "!limuniittitehdas\n!deleteall (Deletes all the messages from the channel.\n" +
                "!play\n!pause\n!resume\n!stop\n!queue\n!food\n!name <@name> <nick>\n!jäähy <@name> <time(secs)>\n" +
                "!mute <@name>");

        message.channel.send({embed: embed});
    }

});

client.on('voiceStateUpdate', async (oldMember, newMember) => {
    let newUserChannel = newMember.voiceChannelID
    let oldUserChannel = oldMember.voiceChannelID

    if(newMember.id in punishList && newUserChannel === "273756161394606081") { // If user joins from punish channel to main channel
        if (punishList[newMember.id] <= new Date().getTime()) {
            delete punishList.newMember;
        } else {
            newMember.setVoiceChannel("291644567860543499"); // Moved back to punish channel if there's time left
        }
    }
});


client.login(config.token);