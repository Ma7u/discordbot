const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const parseString = require("xml2js").parseString;
let currentWeekNumber = require('current-week-number');
let week = currentWeekNumber();


const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;
const weekDay = date.getDay();

function parser (xmlContent) {
    let newtonFood = {};
    parseString(xmlContent, function (err, jsonContent) {
        let json = jsonContent["string"]["_"];
        let contents = JSON.parse(json);
        let foods = contents["MealOptions"];
        try {
            for (let i = 0; i < 2; i++) { // Loop through two food choices
                newtonFood["Linjasto" + i] = [];
                for (let n = 0; n < foods[i]["MenuItems"].length; n++) { // Loop through all that's included in one choice
                    let ingredient = foods[i]["MenuItems"][n]["Name_FI"];
                    if (ingredient) { // Checks if the ingredient is undefined
                        newtonFood["Linjasto" + i].push(ingredient);
                    }
                }
            }
        } catch (error) {
            // Probably weekend, so MenuItems is empty array
            newtonFood["Linjasto0"] = ["-"];
            newtonFood["Linjasto1"] = ["-"];
        }

    });
    // Exports the food object
    //console.log(newtonFood);
    exports.newtonFood = newtonFood
}

function reqListener () { // Gets the xml and sends it to parser function
    let xmlContent = this.responseText;
    parser(xmlContent);
}
let oReq = new XMLHttpRequest();
oReq.addEventListener("load", reqListener);
//oReq.open("GET", "http://www.juvenes.fi/tabid/337/moduleid/1149/RSS.aspx");
oReq.open("GET", "http://www.juvenes.fi/DesktopModules/Talents.LunchMenu/LunchMenuServices.asmx/GetMenuByWeekday?KitchenId=6&MenuTypeId=60&week="+week+"&weekday="+weekDay+"&lang=fi");
oReq.send();



