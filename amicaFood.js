const request = require("request");


request('http://www.amica.fi/modules/json/json/Index?costNumber=0812&language=fi', function(error, response, body) {
    if (error) console.log(error);
    let amicaFood = {};
    const reaktor = JSON.parse(body);
    const menu = reaktor["MenusForDays"][0];
    const dayArray = menu["Date"].slice(0,10).split("-");
    amicaFood["date"] = dayArray[2] + "-" + dayArray[1] + "-" + dayArray[0];

    // Loop through menu and add it to amicaFood, if it's linjasto or iltaruoka
    for (let i = 0; i < menu["SetMenus"].length; i++) {
        const name = menu["SetMenus"][i]["Name"];
        if (name === "Linjasto") {
            amicaFood[name + i] = menu["SetMenus"][i]["Components"];
        } else if (name === "Iltaruoka") {
            amicaFood[name + i] = menu["SetMenus"][i]["Components"];
        }

    }
    if (Object.keys(amicaFood).length === 1) { // If there's no linjasto
        amicaFood["Linjasto0"] = ["-"];
        amicaFood["Linjasto1"] = ["-"];
    }
    if (Object.keys(amicaFood).length === 3) { // If there's no iltaruoka
        amicaFood["Iltaruoka8"] = ["-"];
        amicaFood["Iltaruoka9"] = ["-"];
    }

    // exports the food array
    //console.log(amicaFood);
    exports.amicaFood = amicaFood;
});





